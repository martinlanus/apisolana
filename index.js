const solanaWeb3 = require('@solana/web3.js');
const mysql = require('mysql');
const dotenv = require('dotenv');
const path = require('path')
const nodeCron = require("node-cron");
dotenv.config({ path: path.join(__dirname, './process.env') })

const db = mysql.createConnection({
  host     : process.env.db_host,
  user     : process.env.db_user,
  password : process.env.db_password,
  database : process.env.db_database,
  multipleStatements: true,  
  charset : 'utf8mb4'
});

const asyncDbQuery = (instr) =>{
  return new Promise((resolve, reject)=>{
      db.query(instr,  (error, elements)=>{
          if(error){
              return reject(error);
          }
          return resolve(elements);
      });
  });
};

// Connect to cluster
let connection = new solanaWeb3.Connection(
  'https://solana-api.projectserum.com',
  //solanaWeb3.clusterApiUrl('mainnet-beta'),
  'confirmed',
  //'finalized',    
);

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

// Validators info request function
const getValidatorInfo = async () => {
  console.log("Starting Validator Info request function");
 // Request validator info & populate array for DB insertion
 const CONFIG_PROGRAM_ID = new solanaWeb3.PublicKey('Config1111111111111111111111111111111111111');
 let validatorInfoAccounts = await connection.getParsedProgramAccounts(CONFIG_PROGRAM_ID);
 //  console.log("Validator Info "+JSON.stringify(validatorInfoAccounts[0].account.data.parsed.info.keys[1].pubkey)+"\n");
 let valInfo = [];
 let infoArr = [];
 for (let m=0;m<validatorInfoAccounts.length;m++) {
   valInfo = [];
   //console.log("loop # "+m+" Validator Info "+JSON.stringify(validatorInfoAccounts[m])+"\n");
   // Check current index is validator info data (since infoaccount objects sometimes interpolates buffer and other data)
   if (validatorInfoAccounts[m].account.data.parsed!==undefined && validatorInfoAccounts[m].account.data.parsed.type==="validatorInfo") {
     valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.keys[1].pubkey);
   } else {
     //console.log(validatorInfoAccounts[m].account.data.type);
     continue;
   }
   //(validatorInfoAccounts[m].account.data.parsed.info.keys[1].pubkey===null) ? continue : valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.keys[1].pubkey);
   (validatorInfoAccounts[m].account.data.parsed.info.configData.name!=null) ? valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.configData.name) : valInfo.push('') ;
   (validatorInfoAccounts[m].account.data.parsed.info.configData.keybaseUsername!=null) ? valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.configData.keybaseUsername) : valInfo.push('') ;
   (validatorInfoAccounts[m].account.data.parsed.info.configData.details!=null) ? valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.configData.details) : valInfo.push('') ;
   (validatorInfoAccounts[m].account.data.parsed.info.configData.website!=null) ? valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.configData.website) : valInfo.push('') ;
   infoArr.push(valInfo);
   //console.log("Para validador "+valInfo[0]+" los datos tienen "+valInfo.length+" Loop #"+m+"\n");
   let query = "";
   query = "INSERT INTO validator_info (identity_pk, name, keybaseUsername, details, website) VALUES (?) ON DUPLICATE KEY UPDATE name=VALUES(name), keybaseUsername=VALUES(keybaseUsername), details=VALUES(details), website=VALUES(website) ;";

   try {
     await db.beginTransaction();
     const insertion = await db.query( query , [valInfo] );
     //console.log(insertion);
     await db.commit();
   } catch ( err ) {
     await db.rollback();
     // handle the error
   } finally {
     //console.log("data inserted "+valInfo+"\nwith query: "+query+"\n");
     //await db.close();
   }        
 }
 
}

// Validators metrics request function
const getValidators = async () => {

  // Get current Epoch (for use as max limit to request previous 1, 2 or n epochs)
  let currentEpoch;
  let connected = false
  epochInfoReq: while (connected === false) {
    try {
      currentEpoch = await connection.getEpochInfo();
      console.log("1st connection attempt");
      connected = true;
      break epochInfoReq;      
    } catch ( err ) {
      console.log("Error on 1st connection use ("+err+")\n");
      console.log("Current Epoch is: "+currentEpoch+"\n");
      connection = new solanaWeb3.Connection(
        //'https://solana-api.projectserum.com',
        solanaWeb3.clusterApiUrl('mainnet-beta'),
        //solanaWeb3.clusterApiUrl('mainnet-beta'),
        //solanaWeb3.endpoint('192.168.0.1'),
        //'confirmed',
        'finalized',    
      );
    } finally {
      // console.log("1st connection succesfull");
      // currentEpoch = await connection.getEpochInfo();
      // connected = true;
      // break epochInfoReq;
    }    
  }
  currentEpochNum = parseInt(currentEpoch.epoch);
  let currentSlot = await connection.getSlot();
  currentSlotNum = parseInt(currentSlot);
  let currentTimestamp = (Date.now() / 1000);
  let currentBlockTime = await connection.getBlockTime(currentSlot);
  console.log("current ts is "+currentTimestamp+" and slot ts is "+currentBlockTime+"\n");
  console.log("Current Epoch is: "+JSON.stringify(currentEpoch.epoch)+" & current slot is: "+currentSlotNum+"\n");
  // Instantiate block/slot & time related values for calculation and further requests
  let firstBlock = (432000*(currentEpochNum-1));
  let lastBlock = (432000*currentEpochNum);
  let firstBlockTime;
  let lastBlockTime;
  let blockProd;
  let stakeRewards;
  let encodedPubKeys = [];
  let stakeAccountsKeys = [];

  // Instantiate data object to store parsed information
  let dataArr = [];
  let valData = {};

  // Request vote accounts (validators)
  let account = await connection.getVoteAccounts();

  // Request cluster nodes (for software version)
  let validatorIdentity = account.current[0].nodePubkey;
  let clusterNodes = await connection.getClusterNodes();

  // Request block production to calculate skip rate
  try {
    blockProd = await connection._rpcRequest('getBlockProduction',[{"range":{ "firstSlot":firstBlock,"lastSlot":lastBlock}}]);
    console.log("Trying block production get\n");
  } catch(err) {
    console.log("error en 1er request"+err);
  } finally {
    console.log("Block production obtained\n");
  } 



  // Request timestamps for first and last block of the epoch
  do {
    try {
      firstBlockTime = await connection.getBlockTime(firstBlock);
      (firstBlockTime===null) ? firstBlock++ : firstBlock;
    } catch(err) {
      console.log("Error requesting Block time "+err+"\n");
      firstBlock++;
    }
  } while (firstBlockTime===undefined || firstBlockTime===null);
  do {
    try {  
      lastBlockTime = await connection.getBlockTime(lastBlock);
      (lastBlockTime===null) ? lastBlock++ : lastBlock;
    } catch(err) {
      console.log("Error requesting Block time "+err+"\n");
      lastBlock++;
    }   
  } while (lastBlockTime===undefined || lastBlockTime===null);

  let blockDifference = lastBlock - firstBlock;
  let tsDifference = (lastBlockTime-firstBlockTime)/60/60/24;
  // push block and ts thresholds for current calculations

  console.log("Current ts is: "+Math.floor(+new Date() / 1000)+"\n");

  // Request rewards for stake account pubkeys received (and desired epoch) from getProgramAccounts (maybe group request to avoid API limits)
  let rewardsEpoch = Math.trunc(lastBlock / 432000);

  // Get validators metrics from DDBB to skip validators metrics already loaded
  const validatorsDBquery = "SELECT identity_pk FROM validators WHERE epoch="+currentEpochNum+" AND rewards_epoch="+(currentEpochNum-1)+";";
  let validatorsDB = {};

  // Fetch validators data from DB to skip previously loaded validators' data
  try {  
    const resultElements = await asyncDbQuery(validatorsDBquery);
    validatorsDB = resultElements;
  } catch(err) {
    console.log("Error fetching validators DB "+err+"\n");
  }   

  // Loop through validators (account object) to get needed values and further per-validator requests (ej. stake acounts)
  for (let j=0; j<account.current.length;j++) { 
    // If validators is already in DB results, skip to next
    if (validatorsDB.some(item => item.identity_pk === account.current[j].nodePubkey)) {
      console.log("Validador ("+account.current[j].nodePubkey+") ya existe en DB (Loop #"+j+" of "+account.current.length+")");
      continue;
    } else {
      console.log("Validador NO existe en DB");
    };
    console.log("Loop # "+j+" of "+account.current.length);
    valdata = {};
    valData.epoch = currentEpochNum;
    valData.slot_from = firstBlock;
    valData.slot_to = lastBlock;
    valData.ts_from = firstBlockTime;
    valData.ts_to = lastBlockTime;    
    valData.identity_pk = account.current[j].nodePubkey;
    valData.vote_pk = account.current[j].votePubkey;
    //console.log("Validator identity_pk is "+account.current[j].nodePubkey+" info_pk is "+account.current[j].votePubkey+"\n");
    valData.active_stake = account.current[j].activatedStake;
    (account.current[j].commission===null) ? valData.commission = 0 : valData.commission = account.current[j].commission;
    console.log("Commission is "+account.current[j].commission+"\n");
    valData.last_vote = account.current[j].lastVote;
    valData.root_slot = account.current[j].rootSlot;

    for (let k=0;k<account.current[j].epochCredits.length;k++) {
      (currentEpochNum===account.current[j].epochCredits[k][0]) ? valData.ep_credits = (account.current[j].epochCredits[k][1]-account.current[j].epochCredits[k][2]) : null;
    }
    valData.is_staked = account.current[j].epochVoteAccount;
    leaderSlotsReq: try {
      valData.leader_slots = blockProd.result.value.byIdentity[account.current[j].nodePubkey][0];
    } catch(err) {
      console.log("Error searching Leader slots for pk: "+account.current[j].nodePubkey+"\nError: "+err+"\n");
      valData.leader_slots = 0;
      break leaderSlotsReq;
    }
    blocksProdReq: try {
      valData.blocks_prod = blockProd.result.value.byIdentity[account.current[j].nodePubkey][1];
    } catch(err) {
      console.log("Error searching Block Production for pk: "+account.current[j].nodePubkey+"\nError: "+err+"\n");
      valData.blocks_prod = 0;
      break blocksProdReq;
    }    

    // Loop through cluster nodes to get software version and push it to data object
    for (let l=0;l<clusterNodes.length;l++) {
      if (clusterNodes[l].pubkey===account.current[j].nodePubkey) {
        valData.version = clusterNodes[l].version;
        break;
      }
    }

    // Loop through votePubkeys, encode keys and query stake accounts
    let stakeProgramPubKey = new solanaWeb3.PublicKey('Stake11111111111111111111111111111111111111');
    // Delay request to avoid API throttling
    if (j===0) {
      let firstDelay = 5000;
      await sleep(firstDelay);
      console.log("espere "+firstDelay+" ms");
    } else {
      let secondDelay = 3000;
      await sleep(secondDelay);
      console.log("espere "+secondDelay+" ms");
    };
    let stakeInfo = await connection.getParsedProgramAccounts(stakeProgramPubKey,{"encoding": "jsonParsed","filters": [{"memcmp": {"offset": 124,"bytes":account.current[j].votePubkey}}]});
    console.log("a ver si pasa "+stakeInfo.length);
    valData.q_delegators = stakeInfo.length;

    // Loop through stakeInfo to populate encoded keys array with Decode pubkeys hex to get base 58 format to use in rewards lookup below
    encodedPubKeys = [];
    for (let i=0; encodedPubKeys.length < 5; i++) {
      if (i>20) {break};
      if (stakeInfo[i] != undefined && stakeInfo[i].account != undefined) {
        if (stakeInfo[i].account.data.parsed.info.stake.delegation.activationEpoch < (currentEpochNum-3) && stakeInfo[i].account.data.parsed.info.stake.delegation.deactivationEpoch >= (currentEpochNum)) {
          let encodedPubKey = new solanaWeb3.PublicKey(stakeInfo[i].pubkey._bn);
          encodedPubKeys.push(encodedPubKey.toString());
        }

      }      
    }

    console.log("encodedPubKeys  "+JSON.stringify(encodedPubKeys)+"\n");

    do {  
      try {  
        let stakeRewardsRPC = await connection._rpcRequest('getInflationReward',[encodedPubKeys,{ "commitment": "finalized", "epoch":rewardsEpoch}]);
        stakeRewards = stakeRewardsRPC.result;
        console.log("Stake rewards "+JSON.stringify(stakeRewards)+"\n");
        (stakeRewards===undefined) ? rewardsEpoch-- : rewardsEpoch;
      } catch(err) {
        rewardsEpoch--;
        console.log("Error requesting Rewards "+err+"\n")
      } 
    } while (stakeRewards===undefined || stakeRewards===null);    

    console.log("pase de stake rewards");
    // Calculate APY 
    // Loop through rewards to avoid null values
    let percentReward
    for (const key in stakeRewards) {
      if (stakeRewards[key] !== null) {
        percentReward = stakeRewards[key].amount / (stakeRewards[key].postBalance - stakeRewards[key].amount)
        console.log("Reward Amount "+ stakeRewards[key].amount +"\n");
        console.log("Postbanlance Amount "+ stakeRewards[key].postBalance +"\n");
        console.log("Percentage Change "+ percentReward +"\n");
        console.log("Effective Slot"+stakeRewards[key].effectiveSlot+"\n");
        console.log("Rewards epoch "+ stakeRewards[key].epoch+"\n");
        (stakeRewards[key].epoch===undefined || stakeRewards[key].epoch===null ? valData.rewards_epoch = 0 : valData.rewards_epoch = stakeRewards[key].epoch);  

        break;
      } else {
        console.log("Stake rewards es "+stakeRewards[key]+" en loop #"+key+"\n");
      };
    };
    (valData.rewards_epoch===undefined || valData.rewards_epoch===null ? valData.rewards_epoch = 0 : null ); 
    console.log("Percent rewards es "+percentReward+"\n");
    let epochsPerY = 365.25 / (432000 * tsDifference/blockDifference);
    console.log("Epochs per year "+ epochsPerY +"\n");
    let apy = 0;
    if(stakeRewards != null){
      apy = ((1+percentReward)**epochsPerY)-1;
    };
    (Number.isNaN(apy)) ? apy = 0 : null;
    console.log("APY is "+apy+"\n");
    valData.apy = apy;
    (valData.currentEpochNum > valData.rewards_epoch+1 ? console.log("Detected rewards epoch gap (current epoch: "+valData.currentEpochNum+" rewards epoch: "+valData.rewards_epoch+"\n") : null );
    // After all the data is collected, push to database
    let query = "";
    query = "INSERT INTO validators (epoch, identity_pk, vote_pk, active_stake, commission, ep_credits, is_staked, leader_slots, blocks_prod, version, slot_from, slot_to, ts_from, ts_to, q_delegators, apy, rewards_epoch, last_vote, block_height) VALUES (?) ON DUPLICATE KEY UPDATE active_stake=VALUES(active_stake), commission=VALUES(commission), ep_credits=VALUES(ep_credits), is_staked=VALUES(is_staked), leader_slots=VALUES(leader_slots), blocks_prod=VALUES(blocks_prod), version=VALUES(version), slot_from=VALUES(slot_from), slot_to=VALUES(slot_to), ts_from=VALUES(ts_from), ts_to=VALUES(ts_to),   q_delegators=VALUES(q_delegators), apy=VALUES(apy), rewards_epoch=VALUES(rewards_epoch), last_vote=VALUES(last_vote), block_height=VALUES(block_height) ;";

    let values = "";
    values = [valData.epoch, valData.identity_pk, valData.vote_pk, valData.active_stake, valData.commission, valData.ep_credits, valData.is_staked, valData.leader_slots, valData.blocks_prod, valData.version, valData.slot_from, valData.slot_to, valData.ts_from, valData.ts_to, valData.q_delegators, valData.apy, valData.rewards_epoch, valData.last_vote, valData.block_height];

    try {
      await db.beginTransaction();
      const insertion = await db.query( query , [values] );
      await db.commit();
    } catch ( err ) {
      console.log("Error de insert para un validador "+err);
      await db.rollback();
      // handle the error
    } finally {
      console.log("data inserted "+values+"\nwith query: "+query+"\n");
      //await db.close();
    }     

  }

};

// Schedule a job to run every two minutes
//const job = nodeCron.schedule("*/2 * * * *", getValidators);
console.log("Finished loading "+new Date().toLocaleString()+"\n");
const apyReqSchedule = nodeCron.schedule("0 0 */4 * * *", () => {
  console.log("Starting validators request "+new Date().toLocaleString()+"\n");
  getValidators();
});
  //getValidators();

const valinfoReqSchedule = nodeCron.schedule("0 0 0 * * 5", () => {
  console.log("Starting validators info "+new Date().toLocaleString()+"\n");
  getValidatorInfo();
});