const solanaWeb3 = require('@solana/web3.js');
const mysql = require('mysql');
const dotenv = require('dotenv');
const path = require('path')
const nodeCron = require("node-cron");
dotenv.config({ path: path.join(__dirname, './process.env') })

const db = mysql.createConnection({
  host     : process.env.db_host,
  user     : process.env.db_user,
  password : process.env.db_password,
  database : process.env.db_database,
  multipleStatements: true,  
  charset : 'utf8mb4'
});
  // Connect to cluster
  let connection = new solanaWeb3.Connection(
    solanaWeb3.clusterApiUrl('mainnet-beta'),
    //'confirmed',
    'finalized',    
  );

const getValidatorsHistory = async () => {
  for (i=255;i>254;i--) {
    let pedido = await getValidators(i);
  }  
}

// Validators info request function
const getValidatorInfo = async () => {
  console.log("Starting Validator Info request function");
 // Request validator info & populate array for DB insertion
 const CONFIG_PROGRAM_ID = new solanaWeb3.PublicKey('Config1111111111111111111111111111111111111');
 let validatorInfoAccounts = await connection.getParsedProgramAccounts(CONFIG_PROGRAM_ID);
 //  console.log("Validator Info "+JSON.stringify(validatorInfoAccounts[0].account.data.parsed.info.keys[1].pubkey)+"\n");
 let valInfo = [];
 let infoArr = [];
 for (let m=0;m<validatorInfoAccounts.length;m++) {
   valInfo = [];
   //console.log("loop # "+m+" Validator Info "+JSON.stringify(validatorInfoAccounts[m])+"\n");
   // Check current index is validator info data (since infoaccount objects sometimes interpolates buffer and other data)
   if (validatorInfoAccounts[m].account.data.parsed!==undefined && validatorInfoAccounts[m].account.data.parsed.type==="validatorInfo") {
     valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.keys[1].pubkey);
   } else {
     //console.log(validatorInfoAccounts[m].account.data.type);
     continue;
   }
   //(validatorInfoAccounts[m].account.data.parsed.info.keys[1].pubkey===null) ? continue : valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.keys[1].pubkey);
   (validatorInfoAccounts[m].account.data.parsed.info.configData.name!=null) ? valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.configData.name) : valInfo.push('') ;
   (validatorInfoAccounts[m].account.data.parsed.info.configData.keybaseUsername!=null) ? valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.configData.keybaseUsername) : valInfo.push('') ;
   (validatorInfoAccounts[m].account.data.parsed.info.configData.details!=null) ? valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.configData.details) : valInfo.push('') ;
   (validatorInfoAccounts[m].account.data.parsed.info.configData.website!=null) ? valInfo.push(validatorInfoAccounts[m].account.data.parsed.info.configData.website) : valInfo.push('') ;
   infoArr.push(valInfo);
   //console.log("Para validador "+valInfo[0]+" los datos tienen "+valInfo.length+" Loop #"+m+"\n");
   let query = "";
   query = "INSERT INTO validator_info (identity_pk, name, keybaseUsername, details, website) VALUES (?) ON DUPLICATE KEY UPDATE name=VALUES(name), keybaseUsername=VALUES(keybaseUsername), details=VALUES(details), website=VALUES(website) ;";

   try {
     await db.beginTransaction();
     const insertion = await db.query( query , [valInfo] );
     //console.log(insertion);
     await db.commit();
   } catch ( err ) {
     await db.rollback();
     // handle the error
   } finally {
     //console.log("data inserted "+valInfo+"\nwith query: "+query+"\n");
     //await db.close();
   }        
 }
  

}

// Validators metrics request function
const getValidators = async (inputEpoch) => {
  console.log("Input Epoch is: "+inputEpoch);
  // Get current Epoch (for use as max limit to request previous 1, 2 or n epochs)
  //let currentEpoch = await connection.getEpochInfo();
  let currentEpoch = inputEpoch;
  currentEpochNum = parseInt(inputEpoch);
  //let currentSlot = await connection.getSlot();
  let currentSlot = currentEpochNum*432000;
  console.log("Current slot is: "+currentSlot);
  currentSlotNum = parseInt(currentSlot);
  let currentTimestamp = (Date.now() / 1000);
  //let currentBlockTime = await connection.getBlockTime(currentSlot);
  //console.log("current ts is "+currentTimestamp+" and slot ts is "+currentBlockTime+"\n");
  //console.log("Current Epoch is: "+JSON.stringify(currentEpoch.epoch)+" & current slot is: "+currentSlotNum+"\n");
  // Instantiate block/slot & time related values for calculation and further requests
  let firstBlock = (432000*(currentEpochNum-1));
  let lastBlock = (432000*currentEpochNum);
  let firstBlockTime;
  let lastBlockTime;
  let stakeRewards;
  let encodedPubKeys = [];
  let stakeAccountsKeys = [];

  // Instantiate data object to store parsed information
  let dataArr = [];
  let valData = {};

  // Request vote accounts (validators)
  let account = await connection.getVoteAccounts();
  //console.log("account: "+JSON.stringify(account)+"\n");
  // console.log("type of account: "+typeof account);
  // console.log("account.current\n"+JSON.stringify(account.current[0]));
  // console.log("account current votekey:"+account.current[0].votePubkey);

  // Request cluster nodes (for software version)
  //curl https://api.mainnet-beta.solana.com -X POST -H "Content-Type: application/json" -d '{"jsonrpc": "2.0","id": 1,"method": "getAccountInfo","params": ["87fX6AAJywaQfgMpD9Gkwxpt1e129kKZxBdRG1SQA43m",{"encoding": "jsonParsed"}] }'
  let validatorIdentity = account.current[0].nodePubkey;
  //let validatorPubKey = "8xCBbNBWrchQX888MDY9xaVfPJt3X2paaxjw9u7pdJRk";
  //let validatorPubKey = account.current[0].nodePubkey;
  let clusterNodes = await connection.getClusterNodes();
  // console.log("Cluster nodes info:\n");
  // console.log(clusterNodes[0]);
  // console.log("\n");
 
  // Request block production to calculate skip rate
  // Example request w/parameters: let blockProd = await connection._rpcRequest('getBlockProduction',[{"identity": validatorIdentity, "range":{ "firstSlot":firstBlock,"lastSlot":lastBlock}}]);
  // Example request without params: let blockProd = await connection._rpcRequest('getBlockProduction');
  let blockProd = await connection._rpcRequest('getBlockProduction',[{"range":{ "firstSlot":firstBlock,"lastSlot":lastBlock}}]);
  //console.log("Block production for validator "+account.current[0].nodePubkey+"\n");
    console.log("Block production: "+JSON.stringify(blockProd)+"\n");

  console.log("\n");


  //console.log("Info Validadores parseada: "+JSON.stringify(infoArr)+"\n");
  // Request timestamps for first and last block of the epoch
  do {
    try {
      firstBlockTime = await connection.getBlockTime(firstBlock);
      // console.log("First block time ("+firstBlock+"): "+firstBlockTime+"\n");
      (firstBlockTime===null) ? firstBlock++ : firstBlock;
    } catch(err) {
      console.log("Error requesting Block time "+err+"\n");
      // console.log("First block time: "+firstBlockTime+"\n");
      firstBlock++;
    }
  } while (firstBlockTime===undefined || firstBlockTime===null);
  do {
    try {  
      lastBlockTime = await connection.getBlockTime(lastBlock);
      // console.log("Last block time ("+lastBlock+"):"+lastBlockTime+"\n");
      (lastBlockTime===null) ? lastBlock++ : lastBlock;
    } catch(err) {
      console.log("Error requesting Block time "+err+"\n");
      // console.log("Last block time: "+lastBlockTime+"\n");
      lastBlock++;
    }   
  } while (lastBlockTime===undefined || lastBlockTime===null);

  let blockDifference = lastBlock - firstBlock;
  let tsDifference = (lastBlockTime-firstBlockTime)/60/60/24;
  // push block and ts thresholds for current calculations

  console.log("Current ts is: "+Math.floor(+new Date() / 1000)+"\n");

  // Calculate block difference (needed only if skipped blocks exist) 
  // console.log("Blocks difference "+ blockDifference + "\n" );
  // Calculate timestamp difference in days and epochs per year
  // console.log("Timestamps difference "+tsDifference+"\n" );
  // console.log("First block is "+firstBlock+"\nts for 1st block is "+firstBlockTime+"\nLast block is "+lastBlock+"\n ts for last block is "+lastBlockTime+"\nBlock distance is "+blockDifference+"\nTimestamp difference is "+tsDifference+"\n");

  // Request rewards for stake account pubkeys received (and desired epoch) from getProgramAccounts (maybe group request to avoid API limits)
  let rewardsEpoch = Math.trunc(lastBlock / 432000);
  console.log("Rewards epoch is : "+rewardsEpoch+"\n");


  // Loop through validators (account object) to get needed values and further per-validator requests (ej. stake acounts)
  console.log("accounts lenght is "+account.current.length+"\n");
  for (let j=0; j<account.current.length;j++) { 
    valdata = {};
    valData.epoch = currentEpochNum;
    valData.slot_from = firstBlock;
    valData.slot_to = lastBlock;
    valData.ts_from = firstBlockTime;
    valData.ts_to = lastBlockTime;    
    valData.identity_pk = account.current[j].nodePubkey;
    valData.vote_pk = account.current[j].votePubkey;
    console.log("Current vote pk: "+account.current[j].votePubkey+"\n");
    valData.active_stake = account.current[j].activatedStake;
    (account.current[j].commission===null) ? valData.commission = 0 : valData.commission = account.current[j].commission;
    console.log("Commission is "+account.current[j].commission+"\n");
    valData.last_vote = account.current[j].lastVote;
    valData.root_slot = account.current[j].rootSlot;

    for (let k=0;k<account.current[j].epochCredits.length;k++) {
      //console.log("Comparing current epoch ("+currentEpochNum+") with validator epoch credits ("+account.current[j].epochCredits[k][0]+") validator prev. credits: "+account.current[j].epochCredits[k][2]+" validator new balance "+account.current[j].epochCredits[k][1]+"\n");
      (currentEpochNum===account.current[j].epochCredits[k][0]) ? valData.ep_credits = (account.current[j].epochCredits[k][1]-account.current[j].epochCredits[k][2]) : null;
    }
    valData.is_staked = account.current[j].epochVoteAccount;
    // console.log("Check before error. Loop # "+j+" and search pk is "+account.current[j].nodePubkey+"\n");
    leaderSlotsReq: try {
      valData.leader_slots = blockProd.result.value.byIdentity[account.current[j].nodePubkey][0];

    } catch(err) {
      console.log("Error searching Leader slots "+err+"\n");
      valData.leader_slots = 0;
      break leaderSlotsReq;
    }
    blocksProdReq: try {
      valData.blocks_prod = blockProd.result.value.byIdentity[account.current[j].nodePubkey][1];
    } catch(err) {
      console.log("Error searching Block Production "+err+"\n");
      valData.blocks_prod = 0;
      break blocksProdReq;
    }    
    
    //console.log(blockProd.result.value.byIdentity[account.current[j].nodePubkey][0]);

    // Loop through cluster nodes to get software version and push it to data object
    for (let l=0;l<clusterNodes.length;l++) {
      // console.log("Comparing cluster node identity ("+clusterNodes[l].pubkey+") with validator's identity ("+account.current[j].nodePubkey+"). Current cluster node version is: "+clusterNodes[l].version+"\n");
      // (clusterNodes[l].pubkey===account.current[j].nodePubkey) ? console.log("match con identity pk") : null;
      // (clusterNodes[l].pubkey===account.current[j].votePubkey) ? console.log("match con vote pk") : null;
      if (clusterNodes[l].pubkey===account.current[j].nodePubkey) {
        valData.version = clusterNodes[l].version;
        break;
      }
    }

    // Loop through votePubkeys, encode keys and query stake accounts
    let stakeProgramPubKey = new solanaWeb3.PublicKey('Stake11111111111111111111111111111111111111');
    await sleep(5000);
    let stakeInfo = await connection.getParsedProgramAccounts(stakeProgramPubKey,{"encoding": "jsonParsed","filters": [{"memcmp": {"offset": 124,"bytes":account.current[j].votePubkey}}]});
    // console.log("Stake Accounts Lenght: "+stakeInfo.length+"\n");
    valData.q_delegators = stakeInfo.length;

    // Loop through stakeInfo to populate encoded keys array with Decode pubkeys hex to get base 58 format to use in rewards lookup below
    encodedPubKeys = [];
    // for (let i=0; i < 5; i++) {
    //   // console.log("Stake Accounts:"+JSON.stringify(stakeInfo[i])+"\n");
    //   if (stakeInfo[i] != undefined) {
    //     let encodedPubKey = new solanaWeb3.PublicKey(stakeInfo[i].pubkey._bn);
    //     //console.log(encodedPubKey);
    //     //encodedPubKeys.push(encodedPubKey.toString());
    //     encodedPubKeys.push(encodedPubKey.toString());
    //   }      
    //   // console.log(encodedPubKeys[i]);
    // }
    for (let i=0; encodedPubKeys.length < 20; i++) {
      if (i>40) {break};
    //for (;encodedPubKeys.length > 4 ;) {
      //console.log("Encoded pubkeys lenght is "+encodedPubKeys.length+"\n");


       //console.log("Stake Accounts:"+JSON.stringify(stakeInfo[i])+"\n");
       //console.log("Activation Epoch: "+stakeInfo[i].account.data.parsed.info.stake.delegation.activationEpoch+"\nDeactivation Epoch: "+stakeInfo[i].account.data.parsed.info.stake.delegation.deactivationEpoch+"\n");
      if (stakeInfo[i] != undefined && stakeInfo[i].account != undefined) {
        if (stakeInfo[i].account.data.parsed.info.stake.delegation.activationEpoch < (currentEpochNum-3) && stakeInfo[i].account.data.parsed.info.stake.delegation.deactivationEpoch >= (currentEpochNum)) {
          let encodedPubKey = new solanaWeb3.PublicKey(stakeInfo[i].pubkey._bn);
          console.log("Stake account key "+encodedPubKey+"\n");
          //encodedPubKeys.push(encodedPubKey.toString());
          encodedPubKeys.push(encodedPubKey.toString());
        }

      }      
      // console.log(encodedPubKeys[i]);
    }    
    //console.log("Encoded pubkeys to search rewards from: "+encodedPubKeys+"\n");

    // let encodedPubKey1 = new solanaWeb3.PublicKey(stakeInfo[0].pubkey._bn);
    // console.log("encodedPubKey1  "+encodedPubKey1+"\n");
    // let encodedPubKey2 = new solanaWeb3.PublicKey(stakeInfo[1].pubkey._bn);


    do {  
      try {  
        //console.log("Requesting rewards for Slot "+rewardsEpoch+"\n");
        //stakeRewards = await connection.getInflationReward([encodedPubKeys[0], encodedPubKeys[1], encodedPubKeys[2], encodedPubKeys[3], encodedPubKeys[4]],rewardsEpoch);
        //stakeRewards = await connection.getInflationReward([encodedPubKeys[0], encodedPubKeys[1], encodedPubKeys[2], encodedPubKeys[3], encodedPubKeys[4]],rewardsEpoch);
        //let sr = await connection._rpcRequest('getInflationReward',[[encodedPubKeys[0], encodedPubKeys[1], encodedPubKeys[2], encodedPubKeys[3], encodedPubKeys[4]],{ "epoch":99360000}]);
        //let stakeRewardsRPC = await connection._rpcRequest('getInflationReward',[[encodedPubKeys[0].toString(), encodedPubKeys[1].toString(), encodedPubKeys[2].toString(), encodedPubKeys[3].toString(), encodedPubKeys[4].toString()],{ "commitment": "finalized", "epoch":rewardsEpoch}]);
        let stakeRewardsRPC = await connection._rpcRequest('getInflationReward',[encodedPubKeys,{ "commitment": "finalized", "epoch":rewardsEpoch}]);
        console.log("Stake Rewards RPC response: "+JSON.stringify(stakeRewardsRPC)+"\n");
        stakeRewards = stakeRewardsRPC.result;
    // Example request w/parameters: let blockProd = await connection._rpcRequest('getBlockProduction',[{"identity": validatorIdentity, "range":{ "firstSlot":firstBlock,"lastSlot":lastBlock}}]);

        //console.log("getInflationReward response (from _rpcRequest) "+JSON.stringify(sr)+"\n");
        // console.log("stakeRewards is now: "+stakeRewards+"\n");
        (stakeRewards===undefined) ? rewardsEpoch-- : rewardsEpoch;
        console.log("Rewards epoch is "+rewardsEpoch+"\n");
      } catch(err) {
        rewardsEpoch--;
        console.log("Error requesting Rewards "+err+"\n")
        console.log("stakeRewards is now: "+JSON.stringify(stakeRewards)+"\n"); 
        console.log("Rewards epoch is "+rewardsEpoch+"\n");     
      } 
    } while (stakeRewards===undefined || stakeRewards===null);    
    // console.log("Stake Rewards Response:");
    // console.log(stakeRewards);
    // console.log("\n");

    // Calculate APY 
    // Loop through rewards to avoid null values
    let percentReward
    let maxPostBalance = 0;
    for (const key in stakeRewards) {
      if (stakeRewards[key] !== null && stakeRewards[key].postBalance > maxPostBalance ) {
        maxPostBalance = stakeRewards[key].postBalance;
        percentReward = stakeRewards[key].amount / (stakeRewards[key].postBalance - stakeRewards[key].amount)
        console.log("Reward Amount "+ stakeRewards[key].amount +"\n");
        console.log("Postbanlance Amount "+ stakeRewards[key].postBalance +"\n");
        console.log("Percentage Change "+ percentReward +"\n");
        console.log("Effective Slot"+stakeRewards[key].effectiveSlot+"\n");
        valData.rewards_epoch = stakeRewards[key].epoch;         
        console.log("Rewards epoch "+ stakeRewards[key].epoch+"\n");    
        //break;
      } else {
        console.log("Stake rewards es "+JSON.stringify(stakeRewards[key])+" en loop #"+key+"\n");
      };
      if (stakeRewards.postBalance > 15000000000) { break };
    };
    (valData.rewards_epoch === null) ? valData.rewards_epoch = rewardsEpoch : null;
    console.log("Percent rewards es "+percentReward+"\n");
    let epochsPerY = 365.25 / (432000 * tsDifference/blockDifference);
    console.log("Epochs per year "+ epochsPerY +"\n");
    let apy = 0;
    if(stakeRewards != null){
      apy = ((1+percentReward)**epochsPerY)-1;
    };
    (Number.isNaN(apy)) ? apy = 0 : null;
    // (apy==null) ? console.log("Apy es null "+apy+"\n") : console.log("Apy no es null "+apy+"\n");
    // (apy==NaN) ? console.log("Apy es NaN "+apy+"\n") : console.log("Apy no es NaN "+apy+"\n");
    console.log("Array epoch is "+valData.epoch+". Is Nan check: "+Number.isNaN(valData.epoch)+" Current Epoch # "+currentEpochNum);
    (Number.isNaN(valData.epoch)) ? valData.epoch = valData.rewards_epoch : null;
    // console.log("After NaN check APY is "+apy+"\n");
    // console.log("Apy es del tipo "+typeof apy+"\n");
    console.log("APY is "+apy+"\n");
    valData.apy = apy;
    //dataArr.push(valData);
    //console.log("Parsed validator data is "+JSON.stringify(valData)+"\n");
    // let myArray = Object.entries(dataArr);
    // console.log("Parsed validator array  is "+JSON.stringify(myArray)+"\n");

    //if (j===2) {break};
    // After all the data is collected, push to database
    let query = "";
    query = "INSERT INTO validators (epoch, identity_pk, vote_pk, active_stake, commission, ep_credits, is_staked, leader_slots, blocks_prod, version, slot_from, slot_to, ts_from, ts_to, q_delegators, apy, rewards_epoch, last_vote, block_height) VALUES (?) ON DUPLICATE KEY UPDATE active_stake=VALUES(active_stake), commission=VALUES(commission), ep_credits=VALUES(ep_credits), is_staked=VALUES(is_staked), leader_slots=VALUES(leader_slots), blocks_prod=VALUES(blocks_prod), version=VALUES(version), slot_from=VALUES(slot_from), slot_to=VALUES(slot_to), ts_from=VALUES(ts_from), ts_to=VALUES(ts_to),   q_delegators=VALUES(q_delegators), apy=VALUES(apy), rewards_epoch=VALUES(rewards_epoch), last_vote=VALUES(last_vote), block_height=VALUES(block_height) ;";
    //values = [ ep , slt , val.validators[keyv].identityPubkey , val.validators[keyv].voteAccountPubkey , val.validators[keyv].commission , val.validators[keyv].lastVote , val.validators[keyv].rootSlot , val.validators[keyv].credits , val.validators[keyv].epochCredits , val.validators[keyv].activatedStake , val.validators[keyv].version , val.validators[keyv].delinquent];
    //values.push(val.validators[keyv].skipRate);
    let values = "";
    values = [valData.epoch, valData.identity_pk, valData.vote_pk, valData.active_stake, valData.commission, valData.ep_credits, valData.is_staked, valData.leader_slots, valData.blocks_prod, valData.version, valData.slot_from, valData.slot_to, valData.ts_from, valData.ts_to, valData.q_delegators, valData.apy, valData.rewards_epoch, valData.last_vote, valData.block_height];

    //const db = makeDb( config );
    try {
      await db.beginTransaction();
      const insertion = await db.query( query , [values] );
      //console.log(insertion);
      await db.commit();
    } catch ( err ) {
      await db.rollback();
      // handle the error
    } finally {
      //console.log("data inserted "+values+"\nwith query: "+query+"\n");
      //await db.close();
    }     
    // let conn = await connection.query(query , values);

    // console.log(conn);


  }



};

// Schedule a job to run every two minutes
//const job = nodeCron.schedule("*/2 * * * *", getValidators);
console.log("Finished loading "+new Date().toLocaleString()+"\n");
const apyReqSchedule = nodeCron.schedule("0 0 * * * *", () => {
  console.log("Starting validators request "+new Date().toLocaleString()+"\n");
  getValidators();
});

const valinfoReqSchedule = nodeCron.schedule("0 0 0 * * 5", () => {
  console.log("Starting validators info "+new Date().toLocaleString()+"\n");
  getValidatorInfo();
});

//for (i=246;i<259;i++) {
 // getValidators(245);
//}
getValidatorsHistory();